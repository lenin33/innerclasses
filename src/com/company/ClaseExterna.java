package com.company;

/**
 * Created by lenin on 24/07/17.
 */
public class ClaseExterna {

private int numeroExt = 123;//Atributo de clase externa

   public  class ClaseInterna{
        public void print(){
            System.out.println("Esta es la clase inner");
        }

        public int obtenerNum() {
            System.out.println("Este es el metodo obtenerNumero de la inner class para obb=tener atributos privados de la clase externa");
            return numeroExt;
        }
    }

    public void mostrarInner(){
        ClaseInterna ci = new ClaseInterna();
        ci.print();
    }
}
