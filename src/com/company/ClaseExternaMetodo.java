package com.company;

/**
 * Created by lenin on 24/07/17.
 */
public class ClaseExternaMetodo {

    //Podemos escribir una clase dentro de un metodo y debe de ser de tipo local.
    //Tal como las variables locales la clase queda restringida dentro del metodo.

    void metodoInner(){
        int numero=25;
        class ClaseInner{ //Method Inner Class y es
            public void imprimir(){
                System.out.println("Este el metodo de la inner class "+numero);
            }
        }//Fin de la method inner class

    //Una Method inner Class solo puede ser innicializada dentro del metodo en el que se inicializa.
        ClaseInner metodoInner = new ClaseInner();//Inicializamos a un objeto de la method inner class
        metodoInner.imprimir();//Llamamos al metodo de la method inner class
    }

    //Metodo main.
    public static void main(String []args){
        ClaseExternaMetodo cem = new ClaseExternaMetodo();
        cem.metodoInner();
    }
}
