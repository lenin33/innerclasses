package com.company;

/**
 * Created by lenin on 24/07/17.
 */
abstract class ClaseAnonimaInner {

    public abstract void mymethod();

}


class claseExterna{

    public static void main(String []args){
        // En el caso de clases internas anónimas, las declaramos e instanciamos al mismo tiempo.
//Por lo general, se utilizan cuando se necesita para anular el método de una clase o una interfaz.
        ClaseAnonimaInner inner = new ClaseAnonimaInner() {
            @Override
            public void mymethod() {
                System.out.println("Este es un ejemplo de una clase inner anonima");
            }
        };
        inner.mymethod();
    }
}