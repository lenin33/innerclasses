package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here


        ClaseExterna ce = new ClaseExterna();//Iniziacion de objeto de la clase externa
        System.out.println("Ejecutar metodo de la clase interna");
        ce.mostrarInner();
        System.out.println();


        //Instanciacion de una clase inner.
        //Con el objeto instanciado de la clase externa iniciamos a la clase interna.
        ClaseExterna.ClaseInterna  claseInterna = ce.new ClaseInterna();

        System.out.println(claseInterna.obtenerNum());


    }
}
