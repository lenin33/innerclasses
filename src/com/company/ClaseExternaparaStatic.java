package com.company;

/**
 * Created by lenin on 24/07/17.
 */

    //Al igual que los miembros estáticos, una clase anidada estática no tiene acceso
    // a las variables de instancia y los métodos de la clase externa.
public class ClaseExternaparaStatic {

    static class Clase_Nested{
        public void metodoNested(){
            System.out.println("Esta es la clase Nested");
           }
    }

    public static void main(String []args){
    //No se necesita instanciar a la clase externa para inicializar una clase estática.

        ClaseExternaparaStatic.Clase_Nested clase_nested = new ClaseExternaparaStatic.Clase_Nested();
        clase_nested.metodoNested();

    }
}
